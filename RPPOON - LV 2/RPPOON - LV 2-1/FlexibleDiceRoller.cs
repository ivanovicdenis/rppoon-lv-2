﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_2
{
    class FlexibleDiceRoller : IDiceManager, IDiceRoller
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        public FlexibleDiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }
        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        public void RemoveSpecificDices(int numberOfSides)
        {
                    dice.RemoveAll(die => die.GetNumberOfSides()== numberOfSides);
        }
        public void PrintResultsForeachRoll(IList<int> resultForEachRoll)
        {
            Console.WriteLine("Rezultati bacanja svih kockica: ");
            foreach (int roll in resultForEachRoll)
            {
                Console.Write(roll + ", ");
            }
        }
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
            this.resultForEachRoll
            );
        }
        public int GetDicesCount()
        {
            return dice.Count;
        }
    }
}

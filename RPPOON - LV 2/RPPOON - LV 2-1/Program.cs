﻿using System;
using System.Collections.Generic;

namespace RPPOON___LV_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Die die;
            FlexibleDiceRoller flexibleDiceRoller = new FlexibleDiceRoller();
            flexibleDiceRoller.RemoveAllDice();
            for(int i = 0; i< 10; i++) 
            {
                flexibleDiceRoller.InsertDie(die = new Die(10));
            }
            for (int i = 0; i < 20; i++)
            {
                flexibleDiceRoller.InsertDie(die = new Die(6));
            }
            flexibleDiceRoller.RollAllDice();
            Console.WriteLine("\nRezultati bacanja svih kockica( "+flexibleDiceRoller.GetDicesCount()+" ):");
            flexibleDiceRoller.PrintResultsForeachRoll(flexibleDiceRoller.GetRollingResults());

            
            flexibleDiceRoller.RemoveSpecificDices(6);
            flexibleDiceRoller.RollAllDice();
            Console.WriteLine("\nRezultati kockica nakon uklanjanja kockica sa 6 strana ( " + flexibleDiceRoller.GetDicesCount() + " ) :");
            flexibleDiceRoller.PrintResultsForeachRoll(flexibleDiceRoller.GetRollingResults());

            
            flexibleDiceRoller.RemoveSpecificDices(10);
            flexibleDiceRoller.RollAllDice();
            Console.WriteLine("\nRezultati kockica nakon uklanjanja kockica sa 10 strana ( " + flexibleDiceRoller.GetDicesCount() + " ) :");
            flexibleDiceRoller.PrintResultsForeachRoll(flexibleDiceRoller.GetRollingResults());


        }
    }
}

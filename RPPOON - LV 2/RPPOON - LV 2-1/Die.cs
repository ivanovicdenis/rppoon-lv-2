﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_2
{
    class Die
    {
        private int numberOfSides;
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
        }
        public int Roll()
        {
            int rolledNumber = RandomGenerator.GetInstance().NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }
        public int GetNumberOfSides()
        {
            return this.numberOfSides;
        }
    }

}

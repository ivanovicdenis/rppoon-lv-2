﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON___LV_2
{
    class DiceRoller: ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        private ILogger logger;
        private FileLogger fileLogger;
        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            this.fileLogger = new FileLogger(@"D:\Users\ivano\Desktop\HARPOON\LV 2\RPPOON - LV 2\RPPOON - LV 2\RPPOON - LV 2-1\zapistext.txt");
        }
        public void LogRollingResults()
        {
                fileLogger.Log(this);
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RollAllDice()
        {
            //clear results of previous rolling
            resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                resultForEachRoll.Add(die.Roll());
            }
        }
        public void PrintResultsForeachRoll(IList<int> resultForEachRoll)
        {
            Console.WriteLine("Rezultati bacanja svih kockica: ");
            foreach (int roll in resultForEachRoll)
            {
                Console.WriteLine(roll + ", ");
            }
        }
        //View of the results
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
            this.resultForEachRoll
            );
        }

        public string GetStringRepresentation()
        {
            StringBuilder sb = new StringBuilder();

            foreach (int result in resultForEachRoll)
            {
                sb.Append(result + ", ");
            }
            return sb.ToString();
        }

        public int DiceCount
        {
            get { return dice.Count; }
        }

    }
}
